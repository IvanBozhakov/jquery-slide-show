var slider = function  () {
		$(document).ready(function () {
			$('.img-slide:not(:first)').hide();

			var autoSlide = function () {
				$( '.img-slide:visible' ).hide();
				$('.img-slide:hidden:first').next().animate({
				    left:'250px',
				    width:'toggle'
				  },1000, function() {
				  	$(this).prev().appendTo('#slider').hide();
				   $(this).prependTo('#slider');
				  
				  });
			}

			function moveSlider() {
				$( '.img-slide:visible' ).hide();
				$('.img-slide:hidden:first').animate({
				    left:'250px',
				    width:'toggle'
				  }, 10, function() {
				  	 $(this).prependTo('#slider');
				  	$(this).prev().appendTo('#slider').hide();
				  });
			}
				var autoTimer = setInterval(autoSlide,5000);
			

			$('#nextbtn').on('click',function () {
				 clearInterval(autoTimer);
				$( '.img-slide:visible' ).appendTo('#slider').hide();
				moveSlider();
				autoTimer = setInterval(autoSlide,5000);
			});
			$('#prevbtn').on('click',function () {
				clearInterval(autoTimer);
				$('.img-slide:last ').prependTo('#slider').hide();
				moveSlider();
				autoTimer = setInterval(autoSlide,5000);
			});
			
		});
		
}();